# -*- encoding : utf-8 -*-
class Guest < ActiveRecord::Base
  attr_accessible :address, :city, :country, :email, :first_name, :last_name, :postal, :telefon

  has_many :reservations
  has_many :orders
  has_many :bills
  has_many :payments, through: :bills


  def fullname
  	"#{first_name} #{last_name}"
  end	


end
