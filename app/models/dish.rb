class Dish < ActiveRecord::Base
  attr_accessible :description, :notes, :number, :price, :title

  has_many :orders, as: :item

end
