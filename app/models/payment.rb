class Payment < ActiveRecord::Base
  attr_accessible :bill_id, :card_number, :card_provider, :card_valid_until, :paid_at, :total, :type

  belongs_to :bill
  has_one :guest, through: :bill

end
