class Bill < ActiveRecord::Base
  attr_accessible :discount, :guest_id, :order_sum, :paid, :tax, :total

  belongs_to :guest
  has_one :payment
  has_many :orders

end
