class Order < ActiveRecord::Base
  attr_accessible :bill_id, :cancelled, :discount, :guest_id, :item_id, :item_type, :position, :price, :reservation_id, :product_name

  belongs_to :guest
  belongs_to :reservation
  belongs_to :bill

  belongs_to :item, polymorphic: true

end
