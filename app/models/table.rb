class Table < ActiveRecord::Base
  attr_accessible :exclusive, :number, :reservation_id, :seats, :taken

  belongs_to :reservation

end
