class Reservation < ActiveRecord::Base
  attr_accessible :duration, :guest_id, :notes, :priority, :reservation_date, :special_service

  belongs_to :guest
  has_one :table
  has_many :orders
  

end
