class Drink < ActiveRecord::Base
  attr_accessible :alcoholic, :number, :price, :size, :title

  has_many :orders, as: :item

end
