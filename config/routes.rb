# -*- encoding : utf-8 -*-
Dinner4two::Application.routes.draw do


#--------------------------------------------------------------------------------
# Preloading of STI-Classes, required for development-environment
#--------------------------------------------------------------------------------

  if Rails.env.development?  

    %w[cash_payment credit_card_payment].each do |sti_model|
      require_dependency File.join("app", "models", "#{sti_model}.rb") 
    end  
  end  

#--------------------------------------------------------------------------------
# Delegation of STI-Descendants
# Avoid: undefined method `cash_payment_path' for #<#<Class:0x3a14de0>:0x39e8890>
#--------------------------------------------------------------------------------

  def delegate_descandants klass, controller_name = nil

    controller_name ||= klass.name.demodulize.pluralize.underscore

    klass.descendants.each do |sub_klass|
      resources sub_klass.model_name.demodulize.pluralize.underscore.to_sym, controller: controller_name
    end  
  end  

#--------------------------------------------------------------------------------
# App-Resources
#--------------------------------------------------------------------------------
  
  resources :bills
  resources :orders
  resources :drinks
  resources :dishes
  resources :tables
  resources :reservations
  resources :guests

  resources :payments do
    delegate_descandants CashPayment
    delegate_descandants CreditCardPayment
  end  


#--------------------------------------------------------------------------------


  get "home/index"

  root :to => 'home#index'


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
