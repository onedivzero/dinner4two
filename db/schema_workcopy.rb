# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130723002821) do

  create_table "bills", :force => true do |t|
    t.integer  "guest_id"
    t.decimal  "order_sum",  :precision => 7, :scale => 2, :default => 0.0
    t.decimal  "discount",   :precision => 5, :scale => 2, :default => 0.0
    t.decimal  "total",      :precision => 7, :scale => 2, :default => 0.0
    t.integer  "tax"
    t.boolean  "paid"
    t.datetime "created_at",                                                :null => false
    t.datetime "updated_at",                                                :null => false
  end

  create_table "dishes", :force => true do |t|
    t.integer  "number"
    t.string   "title"
    t.text     "description"
    t.decimal  "price",       :precision => 5, :scale => 2, :default => 0.0
    t.text     "notes"
    t.datetime "created_at",                                                 :null => false
    t.datetime "updated_at",                                                 :null => false
  end

  create_table "drinks", :force => true do |t|
    t.integer  "number"
    t.string   "title"
    t.decimal  "price",      :precision => 5, :scale => 2, :default => 0.0
    t.integer  "size"
    t.boolean  "alcoholic"
    t.datetime "created_at",                                                :null => false
    t.datetime "updated_at",                                                :null => false
  end

  create_table "guests", :force => true do |t|
    t.string   "first_name", :null => false
    t.string   "last_name",  :null => false
    t.string   "address"
    t.integer  "postal"
    t.string   "city"
    t.string   "country"
    t.string   "telefon"
    t.string   "email",      :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "orders", :force => true do |t|
    t.integer  "guest_id"
    t.integer  "reservation_id"
    t.integer  "bill_id"
    t.integer  "item_id"
    t.string   "item_type"
    t.integer  "position"
    t.decimal  "price",          :precision => 7, :scale => 2, :default => 0.0
    t.decimal  "discount",       :precision => 5, :scale => 2, :default => 0.0
    t.boolean  "cancelled"
    t.datetime "created_at",                                                    :null => false
    t.datetime "updated_at",                                                    :null => false
    t.string   "product_name"
  end

  create_table "payments", :force => true do |t|
    t.integer  "bill_id"
    t.string   "type"
    t.decimal  "total",            :precision => 7, :scale => 2, :default => 0.0
    t.datetime "paid_at"
    t.integer  "card_number"
    t.datetime "card_valid_until"
    t.string   "card_provider"
    t.datetime "created_at",                                                      :null => false
    t.datetime "updated_at",                                                      :null => false
  end

  create_table "reservations", :force => true do |t|
    t.integer  "guest_id",                            :null => false
    t.datetime "reservation_date",                    :null => false
    t.integer  "duration",         :default => 60,    :null => false
    t.boolean  "priority",         :default => false, :null => false
    t.boolean  "special_service",  :default => false, :null => false
    t.text     "notes"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
  end

  create_table "tables", :force => true do |t|
    t.integer  "reservation_id"
    t.integer  "number"
    t.integer  "seats"
    t.boolean  "exclusive"
    t.boolean  "taken"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

end
