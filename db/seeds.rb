# -*- encoding : utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


guest_1 = Guest.create(first_name: "Michael", last_name: "Müller", address: "Venloer Str. 23", postal: 50672, city: "Cologne", country: "Germany", telefon: "0221-123456", email: "mm@gmx.de")
guest_2 = Guest.create(first_name: "Thomas", last_name: "Klein", address: "Brühler Str. 55", postal: 50321, city: "Cologne", country: "Germany", telefon: "0221-5154321", email: "tk@gmx.de")
guest_3 = Guest.create(first_name: "Stefanie", last_name: "Hansen", address: "Ruby-Allee 99", postal: 50678, city: "Cologne", country: "Germany", telefon: "0221-545152", email: "sh@gmx.de")
guest_4 = Guest.create(first_name: "Peter", last_name: "Müller", address: "Kennedy-Allee 152", postal: 50321, city: "Bonn", country: "Germany", telefon: "0228-5151125", email: "pm@gmx.de")
guest_5 = Guest.create(first_name: "Marco", last_name: "Hein", address: "Aachener Str. 15", postal: 50678, city: "Cologne", country: "Germany", telefon: "0221-5151", email: "mh@gmx.de")
guest_6 = Guest.create(first_name: "Vera", last_name: "Möckel", address: "Klippe 66", postal: 53332, city: "Bornheim", country: "Germany", telefon: "02222-7153152", email: "vm@gmx.de")
guest_7 = Guest.create(first_name: "Jasmin", last_name: "Capone", address: "Bachstr. 15", postal: 59532, city: "Düsseldorf", country: "Germany", telefon: "0225-1544532", email: "jc@gmx.de")
guest_8 = Guest.create(first_name: "Silke", last_name: "Weier", address: "Körnerstr. 43", postal: 50678, city: "Cologne", country: "Germany", telefon: "0221-9152156", email: "sw_@gmx.de")


table_1 = Table.create(number: 1, seats: 2, taken:false, exclusive:true)
table_2 = Table.create(number: 2, seats: 2, taken:false, exclusive:true)
table_3 = Table.create(number: 3, seats: 4, taken:false, exclusive:true)
table_4 = Table.create(number: 4, seats: 8, taken:false, exclusive:true)

tables = []
seats  = [4,4,2,1,2,3,4,6,8,2,2,4,4,4,2]
(5..20).to_a.each_with_index do |number, index| 
	tables << Table.create(number: number, seats: seats[index], taken:false, exclusive:false)
end


res_1 	= Reservation.create(guest_id: 1, reservation_date: DateTime.new(2013, 7, 17, 20, 00))
res_2 	= Reservation.create(guest_id: 2, reservation_date: DateTime.new(2013, 7, 17, 20, 30), duration: 120)
res_3 	= Reservation.create(guest_id: 3, reservation_date: DateTime.new(2013, 7, 17, 21, 30), duration: 90)

[res_1, res_2, res_3].each_with_index do |res, index|
	res.table = Table.find(index + 1)
end	


dish_1	= Dish.create(number: 1, title: "Pizza Salami", price: 7.50, description: "Italienische Pizza mit Salami, Käse und Oregano", notes: "")
dish_2	= Dish.create(number: 5, title: "Spaghetti Bolognese", price: 6.50, description: "Spaghetti mit Hackfleischsosse vom Rind", notes: "Fleisch aus Bio-Zucht")
dish_3	= Dish.create(number: 8, title: "Chef-Salat mit Putenbrust", price: 6.90, description: "Feldsalat, Tomaten, Gurken, Mais, Putenbrust", notes: "Fleisch aus Bio-Zucht, Produkte von regionalen Bauern")
dish_4	= Dish.create(number: 12, title: "Chicken Tikka Masala", price: 9.80, description: "Indisches Gericht aus dem Tandoori-Ofen", notes: "Nach originalem Rezept aus Goa")
dish_5	= Dish.create(number: 19, title: "Jungschweinrippchen mit Peperoncini und Balsamico-Zwiebeln", price: 16.40, description: "In fein würziger Pfeffersosse", notes: "")
dish_6	= Dish.create(number: 36, title: "Rinderfilet an Kartoffelpüree und Gemüse der Saison", price: 21.30, description: "Verfeinert mit frischen Kräutern", notes: "")
dish_7	= Dish.create(number: 39, title: "Vegetarischer Burger mit Spinat und Kräuterdip", price: 8.60, description: "Auch gänzlich vegan bestellbar", notes: "")
dish_8	= Dish.create(number: 42, title: "Speise-Eis-Kreation nach Art des Hauses", price: 4.20, description: "3 grosse Kugeln frei wählbar mit Sahne", notes: "")


drink_1	= Drink.create(number: 66, title: "Coca Cola", price: 1.20 , size: 250, alcoholic: false)
drink_2	= Drink.create(number: 67, title: "Kölsch vom Fass", price: 1.60 , size: 200, alcoholic: true)
drink_3	= Drink.create(number: 68, title: "Rotwein", price: 4.50 , size: 200, alcoholic: true)
drink_4	= Drink.create(number: 69, title: "Mineralwasser", price: 1.00 , size: 200, alcoholic: false)
drink_5	= Drink.create(number: 70, title: "Kaffee", price: 1.20 , size: 150, alcoholic: false)


# All orders for guest 1
order_1 = Order.create(guest_id: 1, item_id: dish_1.id, item_type: Dish.name, price: dish_1.price, position: 1, reservation_id: 1)
order_2 = Order.create(guest_id: 1, item_id: dish_2.id, item_type: Dish.name, price: dish_2.price, position: 2, reservation_id: 1)
order_3 = Order.create(guest_id: 1, item_id: drink_1.id, item_type: Drink.name, price: drink_1.price, position: 3, reservation_id: 1)
order_4 = Order.create(guest_id: 1, item_id: drink_2.id, item_type: Drink.name, price: drink_2.price, position: 4, reservation_id: 1)

# All orders for guest 2
order_5 = Order.create(guest_id: 2, item_id: dish_3.id, item_type: Dish.name, price: dish_3.price, position: 1, reservation_id: 2)
order_6 = Order.create(guest_id: 2, item_id: dish_4.id, item_type: Dish.name, price: dish_4.price, position: 2, reservation_id: 2)
order_7 = Order.create(guest_id: 2, item_id: drink_1.id, item_type: Drink.name, price: drink_1.price, position: 3, reservation_id: 2)
order_8 = Order.create(guest_id: 2, item_id: drink_3.id, item_type: Drink.name, price: drink_3.price, position: 4, reservation_id: 2)

# All orders for guest 3
order_9 = Order.create(guest_id: 3, item_id: dish_1.id, item_type: Dish.name, price: dish_1.price, position: 1, reservation_id: 3)
order_10 = Order.create(guest_id: 3, item_id: dish_3.id, item_type: Dish.name, price: dish_3.price, position: 2, reservation_id: 3)
order_11 = Order.create(guest_id: 3, item_id: drink_1.id, item_type: Drink.name, price: drink_1.price, position: 3, reservation_id: 3)
order_12 = Order.create(guest_id: 3, item_id: drink_5.id, item_type: Drink.name, price: drink_5.price, position: 4, reservation_id: 3)


# Prepare and create bills
guest_1_orders = guest_1.orders.where(reservation_id: 1)
guest_2_orders = guest_2.orders.where(reservation_id: 2)

order_sum_1	 = guest_1_orders.map(&:price).inject() {|sum, price| sum += price}
order_sum_2	 = guest_2_orders.map(&:price).inject() {|sum, price| sum += price}

guest_1_bill = guest_1.bills.create(order_sum: order_sum_1, total: order_sum_1)
guest_2_bill = guest_2.bills.create(order_sum: order_sum_2, total: order_sum_2)


payment_1 = guest_1_bill.create_payment(total: guest_1_bill.total, paid_at: DateTime.new(2013, 7, 17, 21, 00))
payment_2 = guest_2_bill.create_payment(total: guest_2_bill.total, card_number: 123456, card_valid_until: Date.new(2015,5,31), card_provider: "Visa", paid_at: DateTime.new(2013, 7, 17, 22, 45))


# Create payments, TODO: bill_id does not get filled this way
#payment_1 = guest_1_bill.create_payment(total: guest_1_bill.total, type: CashPayment.name, paid_at: DateTime.new(2013, 7, 17, 21, 00))
#payment_2 = guest_2_bill.create_payment(total: guest_2_bill.total, type: CreditCardPayment.name, card_number: 123456, card_valid_until: Date.new(2015,5,31), card_provider: "Visa", paid_at: DateTime.new(2013, 7, 17, 22, 45))

# payment_1 = CashPayment.create(total: guest_1_bill.total, paid_at: DateTime.new(2013, 7, 17, 21, 00))
# payment_2 = CreditCardPayment.create(total: guest_2_bill.total, card_number: 123456, card_valid_until: Date.new(2015,5,31), card_provider: "Visa", paid_at: DateTime.new(2013, 7, 17, 22, 45))

# payment_1.bill = guest_1_bill
# payment_1.save

# payment_2.bill = guest_2_bill
# payment_2.save