class CreateBills < ActiveRecord::Migration
  def change
    create_table :bills do |t|
      t.integer :guest_id
      t.decimal :order_sum, precision: 7, scale: 2, default: 0
      t.decimal :discount, precision: 5, scale: 2, default: 0
      t.decimal :total, precision: 7, scale: 2, default: 0
      t.integer :tax
      t.boolean :paid

      t.timestamps
    end
  end
end
