class CreateDrinks < ActiveRecord::Migration
  def change
    create_table :drinks do |t|
      t.integer :number
      t.string :title
      t.decimal :price, precision: 5, scale: 2, default: 0
      t.integer :size
      t.boolean :alcoholic

      t.timestamps
    end
  end
end
