class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :guest_id
      t.integer :reservation_id
      t.integer :bill_id
      t.integer :item_id
      t.string :item_type
      t.integer :position
      t.decimal :price, precision: 7, scale: 2, default: 0
      t.decimal :discount, precision: 5, scale: 2, default: 0
      t.boolean :cancelled

      t.timestamps
    end
  end
end
