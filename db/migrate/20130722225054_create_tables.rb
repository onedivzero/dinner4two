class CreateTables < ActiveRecord::Migration
  def change
    create_table :tables do |t|
      t.integer :reservation_id
      t.integer :number
      t.integer :seats
      t.boolean :exclusive
      t.boolean :taken

      t.timestamps
    end
  end
end
