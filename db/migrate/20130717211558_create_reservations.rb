class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.integer :guest_id, null: false
      t.datetime :reservation_date, null: false
      t.integer :duration, null: false, default:60
      t.boolean :priority, null:false, default:false
      t.boolean :special_service, null:false, default:false
      t.text :notes

      t.timestamps
    end
  end
end
