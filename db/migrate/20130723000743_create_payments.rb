class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :bill_id
      t.string :type
      t.decimal :total, precision: 7, scale: 2, default: 0
      t.datetime :paid_at
      t.integer :card_number
      t.datetime :card_valid_until
      t.string :card_provider

      t.timestamps
    end
  end
end
