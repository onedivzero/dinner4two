# -*- encoding : utf-8 -*-
class CreateGuests < ActiveRecord::Migration
  def change
    create_table :guests do |t|
      t.string :first_name,  null: false
      t.string :last_name,  null: false
      t.string :address
      t.integer :postal
      t.string :city
      t.string :country
      t.string :telefon
      t.string :email,  null: false

      t.timestamps
    end
  end
end
