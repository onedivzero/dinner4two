class CreateDishes < ActiveRecord::Migration
  def change
    create_table :dishes do |t|
      t.integer :number
      t.string :title
      t.text :description
      t.decimal :price, precision: 5, scale: 2, default: 0
      t.text :notes

      t.timestamps
    end
  end
end
