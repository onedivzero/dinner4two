require 'test_helper'

class OrdersControllerTest < ActionController::TestCase
  setup do
    @order = orders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:orders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create order" do
    assert_difference('Order.count') do
      post :create, order: { bill_id: @order.bill_id, cancelled: @order.cancelled, discount: @order.discount, guest_id: @order.guest_id, item_id: @order.item_id, item_type: @order.item_type, position: @order.position, price: @order.price, reservation_id: @order.reservation_id }
    end

    assert_redirected_to order_path(assigns(:order))
  end

  test "should show order" do
    get :show, id: @order
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @order
    assert_response :success
  end

  test "should update order" do
    put :update, id: @order, order: { bill_id: @order.bill_id, cancelled: @order.cancelled, discount: @order.discount, guest_id: @order.guest_id, item_id: @order.item_id, item_type: @order.item_type, position: @order.position, price: @order.price, reservation_id: @order.reservation_id }
    assert_redirected_to order_path(assigns(:order))
  end

  test "should destroy order" do
    assert_difference('Order.count', -1) do
      delete :destroy, id: @order
    end

    assert_redirected_to orders_path
  end
end
